# Heterogeneous Systems Research (HEROES) Lab - University of Mississippi #

This is a collection of applications that use our shared work-stealing framework on CPU-GPU platforms.

## List of Applications ##

Binary search tree (searching operations). Given a list of input keys and a binary search tree, the program
outputs whether a key exists in the tree.

Breadth First Search. The program traverses a given graph using both CPU and GPU threads.

## Platform requirements ##

For now, our code works on AMD APU Kaveri platform. Futute support for Shared Virtual Memory (SVM) discrete GPU
systems will be released.