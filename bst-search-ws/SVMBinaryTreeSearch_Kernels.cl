/**********************************************************************
Copyright �2015 Advanced Micro Devices, Inc. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

�	Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
�	Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or
 other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
********************************************************************/
#pragma OPENCL EXTENSION cl_khr_int64_extended_atomics : enable
#define WF_SIZE 64
#define FAILURE 999999999

/* binary tree node definition */
typedef struct nodeStruct
{
  int value;
  __global struct nodeStruct* left;
  __global struct nodeStruct* right;
} node;

/* search keys */
typedef struct searchKeyStruct
{
  int      key;
  __global node* oclNode;
  __global node* nativeNode;
} searchKey;

// processing tasks
void process(__global void* bstRoot, __global void* searchKeyVect, unsigned int index)
{
  __global node*      searchNode  = (__global node *)(bstRoot);
  __global searchKey* keyPtr      = (__global searchKey*)(searchKeyVect); 
  __global searchKey* currKey     = keyPtr + index;

  while(NULL != searchNode)
  {
    if(currKey->key == searchNode->value)
	  {
	    /* rejoice on finding key */
	    currKey->oclNode   = searchNode;
	    searchNode         = NULL;
	  }
    else if(currKey->key < searchNode->value)
	  {
	    /* move left */
	    searchNode = searchNode->left;
	  }
    else
	  {
	    /* move right */
	    searchNode = searchNode->right;
	  }
  }
}

// ==== combine 2 32-bit integers into 1 64-bit number ====
unsigned long combine(unsigned int num1, unsigned int num2){
	return (((unsigned long) num1) << 32) | ((unsigned long) num2);
}

unsigned int getNum1(unsigned long combined){
	return (unsigned int) (combined >> 32);
}

unsigned int getNum2(unsigned long combined){
	unsigned long mask = 0x00000000FFFFFFFF;
	return (unsigned int) (mask & combined);
}
// ====================WORKED==============================

// Head = (index, counter)
// ========== pop 64 tasks per wavefront ==================
// only lane0 thread executes pop()
// return task index in the queue
unsigned int pop(atomic_ulong* head, atomic_uint* tail){
  unsigned int localTail = atomic_load_explicit(tail, 
                                                memory_order_seq_cst, 
                                                memory_scope_all_svm_devices);

	// queue is empty --> return failure
	if (localTail == 0) return FAILURE;

	// try to update the globalTail
  // NOTE localTail is always a multiple of 64 --> no worry about localTail < 0
	localTail -= 64;

	atomic_store_explicit(tail, 
                        localTail, 
                        memory_order_seq_cst, 
                        memory_scope_all_svm_devices);


  // check the globalHead
  unsigned long localHead = atomic_load_explicit(head, 
                                                 memory_order_seq_cst, 
                                                 memory_scope_all_svm_devices);

  // if localTail > global head index 
  // --> we're safe to work on 64 elements starting from the localTail
  // return the localTail as a start working index
  if (localTail > getNum1(localHead)) {
      return localTail; 
  }

  // if we reach here, then
  // either localTail == localHead 
  // or localTail < localHead
  
  // in both cases, set the globalTail to 0
  atomic_store_explicit(tail, 
                        0, 
                        memory_order_seq_cst, 
                        memory_scope_all_svm_devices);

  // prepare to set the globalHead to 0 also
  unsigned long newHead = combine(0, getNum2(localHead) + 1);		

  if (localTail == getNum1(localHead)) {
    // this could be the last block in the queue
    // I'll try to grab this block by CAS
    if (atomic_compare_exchange_strong_explicit(head, &localHead, newHead, 
                                                memory_order_seq_cst, memory_order_relaxed, 
                                                memory_scope_all_svm_devices)) {
      // global head has not been changed,
      // and it has just been reset to 0
      // we can safely work on the task
      return localTail;
    }

    // otherwise, someone has already stolen the last block
  }

  // anyway, we should reset the head to 0
	atomic_store_explicit(head, 
                        newHead, 
                        memory_order_seq_cst, 
                        memory_scope_all_svm_devices);

  // and return FAILURE
	return FAILURE;
}
// ========================================================

// =========== steal 64 tasks from other queue ======
unsigned int steal(atomic_ulong* head, atomic_uint* tail){
	unsigned long oldHead, newHead;
	unsigned taskIndex;

	unsigned long localHead = atomic_load_explicit(head, 
                                                 memory_order_seq_cst, 
                                                 memory_scope_all_svm_devices);

  unsigned int localTail = atomic_load_explicit(tail, 
                                                memory_order_seq_cst, 
                                                memory_scope_all_svm_devices);

	// victim queue has no task left
	if (localTail <= getNum1(localHead)) return FAILURE;

  // otherwise, I could steal at least one task block from the victim 

  // prepare what would be the newHead if I successfully steal a block
	newHead = combine(getNum1(localHead) + 64, getNum2(localHead) + 1);

	if (atomic_compare_exchange_strong_explicit(head, &localHead, newHead, 
                                              memory_order_seq_cst, memory_order_relaxed, 
                                              memory_scope_all_svm_devices)) {
    // successfully stole the block
    // return the start index
	  return getNum1(localHead);
	}

  // otherwise, the victim queue is empty at the time of CAS --> give up
	return FAILURE;
}

/***
 * sample_kernel:
 ***/
__kernel void sample_kernel(__global void* bstRoot,
			                      __global void* searchKeyVect, 
                            global int* workPool,			
			                      global atomic_ulong* heads,			
			                      global atomic_uint* tails,			
                            global unsigned int* startWorkingIndex,
                            const unsigned int numGroups,
                            const unsigned int numTasksPerGroup
){
	// get lane ID
	unsigned int laneID = get_local_id(0) % WF_SIZE;

	// get queue_id
	unsigned int queueID = get_global_id(0) / WF_SIZE;

  unsigned int myQueueStartIdx = queueID * numTasksPerGroup;

  // loop until no job is available
	while (true){
		// laneID 0 thread goes and gets 64 tasks at a time from its own queue
		if (laneID == 0) {
      unsigned int startIdx = pop(heads+queueID, tails+queueID);

      // if there is no task to do 
      if (startIdx == FAILURE) {
        unsigned int victimQueueID = (queueID + 1) % numGroups;
        unsigned int victimQueueStartIdx = victimQueueID * numTasksPerGroup; 

        while (victimQueueID != queueID) {
          startIdx = steal(heads+victimQueueID, tails+victimQueueID);

          if (startIdx != FAILURE) {
            startWorkingIndex[queueID] = startIdx + victimQueueStartIdx;
            break;
          }

          // otherwise, try to steal from another queue
          victimQueueID = (victimQueueID + 1) % numGroups;
          victimQueueStartIdx = victimQueueID * numTasksPerGroup;
        }

        // I have looked at all available queue --> DONE
        if (victimQueueID == queueID)
          startWorkingIndex[queueID] = FAILURE;
      } else {
        startWorkingIndex[queueID] = startIdx + myQueueStartIdx; 
      }
    }

    mem_fence(CLK_GLOBAL_MEM_FENCE);

    unsigned int startIdx = startWorkingIndex[queueID];

    if (startIdx != FAILURE) {
      unsigned int index = workPool[startIdx + laneID];
      process(bstRoot, searchKeyVect, index);
    } else {
      return;
    } 
  }
}
